/*
    Copyright 2019-2020 eMobilify GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
var request = require("request");

var options =
    { method: 'POST',
      url: 'https://token-bridge.shareandcharge.com/new-bridge',
      headers:
          { 'Content-Type': 'application/x-www-form-urlencoded' },
      form:
          { name:"xchf-eth2volta",
            parentUrl:"https://mainnet.infura.io/v3/d28be71ba1df4f05ae9b31612cb76b0e",
            childUrl:"https://volta-rpc.energyweb.org",
            parentTokenAddress:"0xb4272071ecadd69d933adcd19ca99fe80664fc08",
            parentBridgeAddress:"0x359f160c7102E3988f64c157e0f6eC8206d1B2f2",
            childBridgeAddress:"0x9bd65fA8D471ecf34a8b1ABCc4527c20fC7864E1",
            privateKey:"0xBE9D7C16E27BAE9A5B94DD961CBF3A7AC75C83AA6A33D62FA05A3BC57B8E4597"
          }
    };

request(options, function (error, response, body) {
  if (error) {
    console.error(error);
  } else {
    console.log(response);
    console.log(body);
  }
});
