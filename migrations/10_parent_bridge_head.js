/*
    Copyright 2019-2020 eMobilify GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
const ParentBridgeHead = artifacts.require('ParentBridgeHead')

module.exports = function (deployer, network) {
  console.log(network, ('mainnet').indexOf(network))
  // only deploy this contract to poa, kovan and mainnet
  if (('poa - kovan').indexOf(network) > -1) {
    deployer.deploy(ParentBridgeHead, '0xC62e02ddc6C1A78ca63F144253E74c85ecB76B74')
  } else if (('mainnet').indexOf(network) > -1) {
    deployer.deploy(ParentBridgeHead, '0x0000000000000000000000000000000000000000')
  }
}
